#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "assembler.h"
#include "mcp.h"
#include "util.h"

label_list add_label_to_list(label_list list, label new_label) {
    list.length++;
    list.labels = (label*)realloc(list.labels, list.length * sizeof(label));
    list.labels[list.length-1] = new_label;
    return list;
}

label_list create_label_list() {
    label_list list;
    list.length = 0;
    list.labels = malloc(0);
    return list;
}

string_list split_string(char* string, char delimiter) {
    string_list list;
    list.length = 0;
    list.strings = (char**) malloc(0);
    char * part_str;
    char sep[2];
    sep[0] = delimiter;
    sep[1] = 0;

    printf("String to be split: %s\n", string);
    for(part_str = strtok(string, sep);;part_str = strtok(NULL, sep)) {
	if(part_str != NULL) {
	    list.strings = (char**)realloc(list.strings, LIST_STRING_LENGTH * sizeof(char) * (list.length + 1));
	    strcpy((char*)list.strings+(LIST_STRING_LENGTH * list.length), part_str);
	    list.length++;
	}
	else {
	    int i;
	    printf("[start]\toutput list content for testing purposes\n");
	    for(i = 0; i < list.length; i++) {
		printf("\t%s\n", get_split_string_item(list, i));
	    }
	    printf("[end]\toutput list content for testing purposes\n");
	    return list;
	}
    }
    
    return list;
}


char* get_split_string_item(string_list list, int index) {
    if(index > list.length - 1) {
        error("index out of bounce");
        return NULL;
    }

    return (char*)list.strings + (index * LIST_STRING_LENGTH);
}


byte get_regs_bin_code(char* label) {
    
    printf("Label: \"%s\"\n", label);
    if(strcmp(label, "r0") == 0) {
        return 0x00;
    }
    else if(strcmp(label, "r1") == 0) {
        return 0x01;
    }
    else if(strcmp(label, "r2") == 0) {
        return 0x02;
    }
    else if(strcmp(label, "r3") == 0) {
        return 0x03;
    }
    //else
    error("invalid register");
    exit(1);
}
