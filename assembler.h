#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include "mcp.h"

#define LIST_STRING_LENGTH 64

struct s_label {
    char label[256];
    byte address;
};

typedef struct s_label label;

struct s_label_list {
    label * labels;
    int length;
};

typedef struct s_label_list label_list;

label_list add_label_to_list(label_list, label);
label_list create_label_list();


struct s_string_list {
    char ** strings;
    int length;
};

typedef struct s_string_list string_list;

string_list split_string(char*, char);

char* get_split_string_item(string_list, int);

byte get_regs_bin_code(char*);

#endif
