#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "memory.h"
#include "mcp.h"
#include "control.h"
#include "util.h"

byte test_prg2[] = {
              0x04, 0x00, 0x02,
              0x04, 0x01, 0x03,
              0x10, 0x00, 0x01,
              0x16, 0x00, 0x00,
              0x18, 0x00, 0x01,
              0x17, 0x02, 0x00,
              0x01, 0x00, 0x00,
              0x02, 0x00, 0x00,
              0x03, 0x00, 0x00,
              0x01, 0x00, 0x00,
              0x00, 0x00, 0x00
};


byte test_prg[] = {
                   0x04, 0x00, 0x7F,
                   0x04, 0x02, 0x01,
                   0x04, 0x01, 0x20,
                   0x14, 0x01, 0x00,
                   0x16, 0x01, 0x00,
                   0x18, 0x01, 0x01,
                   0x08, 0x01, 0x00,
                   0x10, 0x01, 0x02,
                   0x0F, 0x01, 0x00,
                   0x0A, 0x06, 0x00,
                   0x01, 0x00, 0x00,
                   0x00, 0x00, 0x00
};

int main(int argc, char** argv) {

    virtual_store store = init();
    int exit_code = 0;
    
    if(argc < 2) {
	warning("no file given");
        info("load test program");
        store.memory = test_prg;
    }
    else {
	info("load from file:");
	info(argv[1]);
        store.memory = load_file(argv[1]);
    }
    exit_code = (int) execute(store);
    
    return exit_code;
}

byte * load_file(char* filename) {
    FILE* file = fopen(filename, "rb");
    byte * data;
    struct stat fileStat;
    stat(filename,&fileStat);

    data = malloc(fileStat.st_size);

    fread(data, 1, fileStat.st_size, file);
    
    fclose(file);

    return data;
}


virtual_store init() {
    virtual_store store;

    store.instr_pointer = 0;
    store.mem_offset = 0;
    store.regs[0] = 0;
    store.regs[1] = 0;
    store.regs[2] = 0;
    store.regs[3] = 0;

    return store;
}
