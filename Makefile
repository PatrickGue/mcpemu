CC=gcc
CFLAGS=-Wall
PROG1=mcp
SRCS1=mcp.c control.c alu.c util.c


PROG2=mcpa
SRCS2=mcpa.c util.c assembler.c

all:${PROG1} ${PROG2}


${PROG1}:${SRCS1}
	${CC} ${CFLAGS} -o ${PROG1} ${SRCS1}

${PROG2}:${SRCS2}
	${CC} ${CFLAGS} -o ${PROG2} ${SRCS2}

clean:
	rm ${PROG1} ${PROG2}
