                        ==== Microprocesser Emulator ====

== Emulator Runtime ==

*Instruction set*

Each instruction is 3 bytes long:
1) Instruction code
2) Argument 1
3) Argument 2

'label'      'bin code' 'arg 1'  'arg 2' 'description'
 halt         0x00       -        -       Stop program
 dumm         0x01       -        -       Dump current memory block
 dumr         0x02       -                Dump register
 dumi         0x03       -        -       Dump instruction pointer
 
 morv         0x04       regs     value   Move value to register from value
 morm         0x05       regs     addr    Move value to register from memory
 morr         0x06       regs     regs    Move value to register from register
 momm         0x07       addr     addr    Move value to memory from memory
 momr         0x08       addr     regs    Move value to memory from register
 ador         0x09       regs             Switch memory address offset by value
                                          in register

 jmpv         0x0A       value    -       Jump to specific address
 jmpr         0x0B       regs     -       Jump to address saved in register
 jmpm         0x0C       addr     -       Jump to address saved in address

 equa         0x0D       regs     regs    Go to next line if regs are equal,
                                          skip if not 
 grea         0x0E       regs     regs    Go to next line if fist regs is
                                          greater than second, skip if not
 less         0x0F       regs     regs    Go to next line if fist regs is
                                          smaller than second, skip if not

 cadd         0x10       regs     regs    Add second regs to first regs
 csub         0x11       regs     regs    Subtract second regs from first regs
 cmul         0x12       regs     regs    Multiplicate second regs with first
                                          regs
 cdiv         0x13       regs     regs    Divide second regs with first regs

 prcr         0x14       regs     -       Print ASCII value from register
 incr         0x15       regs     -       Save ASCII input from stdin to regs
 prnr         0x16       regs     -       Print number value from register
 innr         0x17       regs     -       Save number input from stdin to regs

 mmrr         0x18       regs     regs    Move value to memory at register from
                                          register
 mrrm         0x19       regs     regs    Move value to register from memory at
                                          register

*Memory*

The memory holds 64KiB. There are four registers: r0, r1, r2, r3, each holding
one byte.
As the move commands can only hold one byte as an argument, only 2^8 (256) bytes
can be accessed at the same time. To access the full memory, the command adro
(0x09) can be called to set an offset, which means 2^8 * 2^8 (256 * 256 = 65536)
bytes.

== Assembler ==

*Additional instructions*

These instructions are not part of the actual instruction set but will be
compiled to work with it

'label'      'bin code' 'arg 1'  'arg 2' 'description'
 .{label}     -          -        -       Set label where to jump to
 jmpl         -          {label}  -       Jump to label, will be compiled to
                                          jmpv with address saved in label
 inst         -          value            manually modify instruction pointer
                                          to write instructions at specific
                                          memory location

== Tested Platforms ==

Linux	    RHEL      6.9	gcc
Apple macOS           10.13	gcc
FreeBSD		      11.1	clang, gcc
