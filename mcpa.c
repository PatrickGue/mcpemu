#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include "mcp.h"
#include "mcpa.h"
#include "util.h"
#include "assembler.h"



int main(int argc, char** argv) {
    int exit_code = 0;
    
    if(argc < 2) {
	error("no input and output file");
        exit_code = 1;
    }
    else if(argc < 3) {
        error("no output file");
        exit_code = 2;
    }
    else {
        int index = 0;
        int instr_pointer = 0, highest_instr_pointer = 0;
        char * program_code = (char*) load_file(argv[1]);

        string_list str_list = split_string(program_code, '\n');
        
        label_list lbl_list = create_label_list();

        for(index = 0; index < str_list.length; index++) {
            if(get_split_string_item(str_list, index)[0] == '.') {
                label lbl;
                lbl.address = instr_pointer;
                strcpy(lbl.label, get_split_string_item(str_list, index)+1);
                lbl_list = add_label_to_list(lbl_list, lbl);
            }
            else {
                instr_pointer += 3;
            }
        }
        info("labels");
        for(index = 0; index < lbl_list.length; index++) {
            printf("%s 0x%02x\n", lbl_list.labels[index].label, lbl_list.labels[index].address);
        }

        byte * program_bin = malloc(256 * 256 * sizeof(byte));
        instr_pointer = 0;
        for(index = 0; index < str_list.length; index++) {
            if(get_split_string_item(str_list, index)[0] != '.') {
                info(get_split_string_item(str_list, index));
                string_list command_split = split_string(get_split_string_item(str_list,index), ' ');
                printf("instr_pointer: %i\n", instr_pointer);
                if(strcmp(get_split_string_item(command_split,0), "halt") == 0) {
                    info("instr: halt");
                    program_bin[(int)instr_pointer] = 0x00;
                    program_bin[(int)instr_pointer+1] = 0x00;
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "dumm") == 0) {
                    info("instr: dumm");
                    program_bin[(int)instr_pointer] = 0x01;
                    program_bin[(int)instr_pointer+1] = 0x00;
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "dumr") == 0) {
                    info("instr: dumr");
                    program_bin[(int)instr_pointer] = 0x02;
                    program_bin[(int)instr_pointer+1] = 0x00;
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "dumi") == 0) {
                    info("instr: dumi");
                    program_bin[(int)instr_pointer] = 0x03;
                    program_bin[(int)instr_pointer+1] = 0x00;
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "morv") == 0) {
                    info("instr: morv");
                    printf("%s %s\n", get_split_string_item(command_split,1), get_split_string_item(command_split,2));
                    program_bin[(int)instr_pointer] = 0x04;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = (byte)strtol(get_split_string_item(command_split,2),NULL,16);
                }
                else if(strcmp(get_split_string_item(command_split,0), "morm") == 0) {
                    info("instr: morm");
                    program_bin[(int)instr_pointer] = 0x05;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = (byte)strtol(get_split_string_item(command_split,2),NULL,16);
                }
                else if(strcmp(get_split_string_item(command_split,0), "morr") == 0) {
                    info("instr: morr");
                    program_bin[(int)instr_pointer] = 0x06;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "momm") == 0) {
                    info("instr: momm");
                    program_bin[(int)instr_pointer] = 0x07;
                    program_bin[(int)instr_pointer+1] = (byte)strtol(get_split_string_item(command_split,1),NULL,16);
                    program_bin[(int)instr_pointer+2] = (byte)strtol(get_split_string_item(command_split,2),NULL,16);
                }
                else if(strcmp(get_split_string_item(command_split,0), "momr") == 0) {
                    info("instr: momr");
                    program_bin[(int)instr_pointer] = 0x08;
                    program_bin[(int)instr_pointer+1] = (byte)strtol(get_split_string_item(command_split,1),NULL,16);
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "ador") == 0) {
                    info("instr: ador");
                    program_bin[(int)instr_pointer] = 0x09;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "jmpv") == 0) {
                    info("instr: jmpv");
                    program_bin[(int)instr_pointer] = 0x0A;
                    program_bin[(int)instr_pointer+1] = (byte)strtol(get_split_string_item(command_split,1),NULL,16);
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "jmpl") == 0) {
                    int i = 0;
                    info("instr: jmpl / jmpv");
                    program_bin[(int)instr_pointer] = 0x0A;
                    for(i = 0; i < lbl_list.length; i++) {
                        if(strcmp(get_split_string_item(command_split,1), lbl_list.labels[i].label) == 0) {
                            program_bin[(int)instr_pointer+1] = lbl_list.labels[i].address;
                        }
                    }
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "jmpr") == 0) {
                    info("instr: jmpr");
                    program_bin[(int)instr_pointer] = 0x0B;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "jmpm") == 0) {
                    info("instr: jmpm");
                    program_bin[(int)instr_pointer] = 0x0C;
                    program_bin[(int)instr_pointer+1] = (byte)strtol(get_split_string_item(command_split,1),NULL,16);
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "equa") == 0) {
                    info("instr: equa");
                    program_bin[(int)instr_pointer] = 0x0D;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "grea") == 0) {
                    info("instr: grea");
                    program_bin[(int)instr_pointer] = 0x0E;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "less") == 0) {
                    info("instr: less");
                    program_bin[(int)instr_pointer] = 0x0F;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "cadd") == 0) {
                    info("instr: cadd");
                    program_bin[(int)instr_pointer] = 0x10;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "csub") == 0) {
                    info("instr: csub");
                    program_bin[(int)instr_pointer] = 0x11;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "cmul") == 0) {
                    info("instr: cmul");
                    program_bin[(int)instr_pointer] = 0x12;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "cdiv") == 0) {
                    info("instr: cdiv");
                    program_bin[(int)instr_pointer] = 0x13;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "prcr") == 0) {
                    info("instr: prcr");
                    program_bin[(int)instr_pointer] = 0x14;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "incr") == 0) {
                    info("instr: incr");
                    program_bin[(int)instr_pointer] = 0x15;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "prnr") == 0) {
                    info("instr: prnr");
                    program_bin[(int)instr_pointer] = 0x16;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "innr") == 0) {
                    info("instr: innr");
                    program_bin[(int)instr_pointer] = 0x17;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = 0x00;
                }
                else if(strcmp(get_split_string_item(command_split,0), "mmrr") == 0) {
                    info("instr: mmrr");
                    program_bin[(int)instr_pointer] = 0x18;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split,0), "mrrm") == 0) {
                    info("instr: mrrm");
                    program_bin[(int)instr_pointer] = 0x19;
                    program_bin[(int)instr_pointer+1] = get_regs_bin_code(get_split_string_item(command_split,1));
                    program_bin[(int)instr_pointer+2] = get_regs_bin_code(get_split_string_item(command_split,2));
                }
                else if(strcmp(get_split_string_item(command_split, 0), "inst") == 0) {
                    instr_pointer = strtol(get_split_string_item(command_split, 1), NULL, 16) - 3;
                }
                instr_pointer += 3;
                if(instr_pointer > highest_instr_pointer)
                    highest_instr_pointer = instr_pointer;
            }
        }

        for(index = 0; index < instr_pointer; index++) {
            printf("%3i: 0x%02x\n", index, program_bin[index]);
        }

        printf("save \"%s\"\n", argv[2]);
        save_file(argv[2], program_bin, instr_pointer);
        printf("file \"%s\" saved\n", argv[2]);
        
    }
    return exit_code;
}


byte * load_file(char* filename) {
    FILE* file = fopen(filename, "r");
    byte * data;
    struct stat fileStat;
    stat(filename,&fileStat);

    data = malloc(fileStat.st_size);

    fread(data, 1, fileStat.st_size, file);
    
    fclose(file);

    return data;
}



bool save_file(char* filename, byte* data, int size) {
    FILE* file = fopen(filename, "r+");
    if(!file)
        return false;
    fwrite(data, sizeof(byte), size, file);
    fclose(file);
    return true;
}
