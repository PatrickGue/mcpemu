#ifndef CONTROL_H
#define CONTROL_H

#define HALT 0x00
#define DUMM 0x01
#define DUMR 0x02
#define DUMI 0x03
#define MORV 0x04
#define MORM 0x05
#define MORR 0x06
#define MOMM 0x07
#define MOMR 0x08
#define ADOR 0x09
#define JMPV 0x0A
#define JMPR 0x0B
#define JMPM 0x0C
#define EQUA 0x0D
#define GREA 0x0E
#define LESS 0x0F
#define CADD 0x10
#define CSUB 0x11
#define CMUL 0x12
#define CDIV 0x13
#define PRCR 0x14
#define INCR 0x15
#define PRNR 0x16
#define INNR 0x17
#define MMRR 0x18
#define MRRM 0x19


#include "memory.h"

/* TODO: use instances of s_instruction in control.c instead of #define 
         statements */
struct s_instruction {
    byte code;
    char name[5];
};

typedef struct s_instruction instruction;

byte execute(virtual_store);

#endif
