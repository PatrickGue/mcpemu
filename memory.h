#ifndef MEMORY_H
#define MEMORY_H

/* random access memory holds 64K */
#define MEMORY_SIZE 65536

typedef char byte;


struct s_virtual_store {
    byte instr_pointer;
    byte mem_offset;
    
    byte regs[4];
    
    byte * memory;
};

typedef struct s_virtual_store virtual_store;



#endif
