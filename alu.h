#ifndef ALU_H
#define ALU_H

#include "mcp.h"

struct s_result {
    byte result;
    bool overflow;
};


typedef struct s_result result;


result add(byte, byte);
result sub(byte, byte);
result mul(byte, byte);
result div(byte, byte);

bool equa(byte, byte);
bool grea(byte, byte);
bool less(byte, byte);

#endif
