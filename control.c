#include <stdio.h>
#include "control.h"
#include "memory.h"
#include "mcp.h"
#include "alu.h"

byte execute(virtual_store store) {
    bool halt = false;
    result res;
    int index;
    byte input, exit_code;
    
    while(halt == false) {
        byte instr = store.memory[(store.mem_offset * 256) + store.instr_pointer];
        byte arg1 = store.memory[(store.mem_offset * 256) + store.instr_pointer + 1];
        byte arg2 = store.memory[(store.mem_offset * 256) + store.instr_pointer + 2];


        switch(instr) {
        case HALT:
            halt=true;
            exit_code = 0;
            break;
        case DUMM:
            printf("MEMORY BLOCK: %i\n",store.mem_offset);
            for(index = 0; index < 256; index++) {
                if(index % 8 == 0) {
                    printf("\n%5i: ", index);
                }
                printf("0x%02x ",store.memory[(store.mem_offset * 256) + index]);
            }
            printf("\n\n");
            break;
        case DUMR:
            printf("REGISTERS:\n");
            for(index = 0; index < 4; index++) {
                printf("%i: 0x%02x\n", index, store.regs[index]);
            }
            printf("\n");
            break;
        case DUMI:
            printf("INSTRUCTION POINTER:\n0x%02x\n",store.instr_pointer);
            break;
        case MORV:
            store.regs[(int)arg1] = arg2;
            break;
        case MORM:
            store.regs[(int)arg1] = store.memory[(store.mem_offset * 256) + arg2];
            break;
        case MORR:
            store.regs[(int)arg1] = store.regs[(int)arg2];
            break;
        case MOMM:
            store.memory[(int)(store.mem_offset * 256) + arg1] = store.memory[(int)(store.mem_offset * 256) + arg2];
            break;
        case MOMR:
            store.memory[(int)(store.mem_offset * 256) + arg1] = store.regs[(int)arg2];
            break;
        case ADOR:
            store.mem_offset = arg1;
            break;
        case JMPV:
            store.instr_pointer = arg1 - 3;
            break;
        case JMPR:
            store.instr_pointer = store.regs[(int)arg1] - 3;
            break;
        case JMPM:
            store.instr_pointer = store.memory[(int)(store.mem_offset * 256) + arg1] -3;
            break;
        case EQUA:
            if(equa(store.regs[(int)arg1],store.regs[(int)arg2]) == false) {
                store.instr_pointer += 3;
            }
            break;
        case GREA:
            if(grea(store.regs[(int)arg1],store.regs[(int)arg2]) == false) {
                store.instr_pointer += 3;
            }
            break;
        case LESS:
            if(less(store.regs[(int)arg1],store.regs[(int)arg2]) == false) {
                store.instr_pointer += 3;
            }
            break;
        case CADD:
            res = add(store.regs[(int)arg1], store.regs[(int)arg2]);
            store.regs[(int)arg1] = res.result;
            break;
        case CSUB:
            res = sub(store.regs[(int)arg1], store.regs[(int)arg2]);
            store.regs[(int)arg1] = res.result;
            break;
        case CMUL:
            res = mul(store.regs[(int)arg1], store.regs[(int)arg2]);
            store.regs[(int)arg1] = res.result;
            break;
        case CDIV:
            res = div(store.regs[(int)arg1], store.regs[(int)arg2]);
            store.regs[(int)arg1] = res.result;
            break;
        case PRCR:
            printf("%c", store.regs[(int)arg1]);
            break;
        case INCR:
            store.regs[(int)arg1] = getchar();
            break;
        case PRNR:
            printf("%d\n", (int)store.regs[(int)arg1]);
            break;
        case INNR:
            scanf("%d",(int*) &input);
            store.regs[(int)arg1] = input;
            break;
        case MMRR:
            store.memory[(int)(store.mem_offset * 256) + store.regs[(int)arg1]] = store.regs[(int)arg2];
            break;
        case MRRM:
            store.regs[(int)arg1] = store.memory[(int)(store.mem_offset * 256) + store.regs[(int)arg2]];
            break;
        }

        store.instr_pointer += 3;
    }
    return exit_code;
}
