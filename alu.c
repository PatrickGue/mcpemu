#include "alu.h"
#include "mcp.h"

result add(byte a, byte b) {
    result res;
    if(a + b < a) {
        res.overflow = true;
    }
    res.result = a + b;
    return res;
}


result sub(byte a, byte b) {
    result res;
    if(a - b < 0) {
        res.overflow = true;
    }
    res.result = a - b;
    return res;
}

result mul(byte a, byte b) {
    result res;
    if(a * b < a) {
        res.overflow = true;
    }
    res.result = a * b;
    return res;
}

result div(byte a, byte b) {
    result res;
    res.overflow = false;
    res.result = a / b;
    return res;
}


bool equa(byte a, byte b) {
    return a == b;
}

bool grea(byte a, byte b) {
    return a > b;
}

bool less(byte a, byte b) {
    return a < b;
}
