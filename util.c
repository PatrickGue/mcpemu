#include <stdio.h>
#include "util.h"

#define ERR_MESSAGE  "[error]   "
#define LOG_MESSAGE  "[log]     "
#define WARN_MESSAGE "[warning] "

void error(char* message) {
    printf("%s%s\n", ERR_MESSAGE, message);
}

void info(char* message) {
    printf("%s%s\n", LOG_MESSAGE, message);
}

void warning(char* message) {
    printf("%s%s\n", LOG_MESSAGE, message);
}
